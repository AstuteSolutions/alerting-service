using System;
using System.Collections.Generic;
using Alerting.Service.AppService.Services;
using Alerting.Service.AppService.Services.Contracts;
using Alerting.Service.Repository.Repositories.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Iper.ReportBuilder.Api.Web.Client.Contracts;
using Microsoft.Extensions.Logging;
using Program = Alerting.Service.Workers.Program;
using System.Threading.Tasks;

namespace Alerting.Service.AppService.Test
{
    [TestClass]
    public class ServicesTest
    {
        private PercolatorProcessService _percolatorProcessService;
        private IEmailService _emailService;

        [TestInitialize]
        public void Init()
        {
            var services = Program.CreateHostBuilder(Array.Empty<string>()).Build().Services;
            var logger = (ILogger<PercolatorProcessService>)services.GetService(typeof(ILogger<PercolatorProcessService>));
            var surveyRepository = (ISurveyRepository)services.GetService(typeof(ISurveyRepository));
            var percolatorService = (IPercolatorService)services.GetService(typeof(IPercolatorService));
            _emailService = (IEmailService)services.GetService(typeof(IEmailService));
            _percolatorProcessService = new PercolatorProcessService(percolatorService, logger, surveyRepository, _emailService);
        }

        [TestMethod]
        public async Task PercolatorProcessService_Process_Test()
        {
            try
            {
                await _percolatorProcessService.Process();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [TestMethod]
        public async Task SendEmail_Service_Test()
        {
            var templateId = 40;
            var placeHolderKeyValuePair = new Dictionary<string, string>
            {
                {"[ProjectNumber]", "124018"},
                {"[SurveyName]", "Test name"},
                {"[CustomerName]", "surveyInfo.CompanyName"}
            };

            try
            {
                await _emailService.SendEmail(templateId, placeHolderKeyValuePair);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
