﻿namespace Alerting.Service.Domain.Options
{
    public class ReportBuilderIdentityOptions
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string RootUrl { get; set; }

    }
}
