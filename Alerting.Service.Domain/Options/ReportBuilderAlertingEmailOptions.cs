﻿namespace Alerting.Service.Domain.Options
{
    public class ReportBuilderAlertingEmailOptions
    {
        public int TypeId { get; set; }
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public string FromName { get; set; }
    }
}
