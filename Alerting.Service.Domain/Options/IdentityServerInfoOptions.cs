﻿namespace Alerting.Service.Domain.Options
{
    public class IdentityServerInfoOptions
    {
        public string ApiGatewayUrl { get; set; }
        public string IdentityServerUrl { get; set; }
        public string AesKey { get; set; }
        public bool DisableApiGateway { get; set; }
        public bool DisableIdentityServer { get; set; }

    }
}
