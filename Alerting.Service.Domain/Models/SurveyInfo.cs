﻿namespace Alerting.Service.Domain.Models
{
    public class SurveyInfo
    {
        public int SurveyId { get; set; }
        public string SurveyName { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
    }
}
