﻿namespace Alerting.Service.Domain.Models
{
    public class Email
    {
        public int? TemplateId { get; set; }
        public int TypeId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public string FromName { get; set; }
        public int? UserId { get; set; }

    }
}
