using System;
using System.Threading;
using System.Threading.Tasks;
using Alerting.Service.AppService.Services.Contracts;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Alerting.Service.Workers.Workers
{
    public class ReportBuilderPercolatorWorker : IHostedService
    {
        private readonly ILogger<ReportBuilderPercolatorWorker> _logger;
        private readonly IPercolatorProcessService _percolatorProcessService;
        private Timer _timer;

        public ReportBuilderPercolatorWorker(ILogger<ReportBuilderPercolatorWorker> logger, IPercolatorProcessService percolatorProcessService)
        {
            _logger = logger;
            _percolatorProcessService = percolatorProcessService;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMinutes(60));
            return Task.CompletedTask;
        }

        private async void DoWork(object state)
        {
            try
            {
                await _percolatorProcessService.Process();
            }
            catch (Exception e)
            {
                _logger.LogError("Error in ReportBuilderPercolatorWorker", e);
            }

        }
        
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            Dispose();
            return Task.CompletedTask;
        }
        public  void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
