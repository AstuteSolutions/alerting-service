using System;
using System.IO;
using Alerting.Service.Workers.Workers;
using Iper.Common.Core.Logging;
using Iper.Common.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Alerting.Service.Workers
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(builder =>
                {
                    var settingsLocation = AppDomain.CurrentDomain.BaseDirectory ?? string.Empty;
                    builder.AddJsonFile(
                        Path.Combine(settingsLocation, "appSettings.json"),
                        optional: false, reloadOnChange: true);
                })
                .ConfigureLogging((context, builder) =>
                {
                    builder.AddIperLogWriterWithConfig<ElasticSearchLogger, RabbitMqConnectionInfo>(context.Configuration.GetSection("LoggerRabbitMqConnectionInfo"));
                    builder.AddIperLogger(context.Configuration["Environment"], Applications.ReportBuilderApi);
                })
                .ConfigureServices((context, services) =>
                {
                    services.AddConfigurations(context.Configuration);
                    services.RegisterReportBuilderService(services.BuildServiceProvider());
                    services.AddServices();

                    services.AddHostedService<ReportBuilderPercolatorWorker>();
                });

    }
}
