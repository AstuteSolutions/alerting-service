﻿using System;
using Alerting.Service.AppService.Services;
using Alerting.Service.AppService.Services.Contracts;
using Alerting.Service.Domain.Options;
using Alerting.Service.Repository.Repositories;
using Alerting.Service.Repository.Repositories.Contracts;
using Iper.Common.Core.Logging;
using Iper.Common.EntityFramework;
using Iper.ReportBuilder.Api.Web.Client;
using Iper.ReportBuilder.Api.Web.Client.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Alerting.Service.Workers
{
    public static class ServiceCollectionExtensions
    {

        public static void AddConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<IISServerOptions>(options =>
            {
                options.AutomaticAuthentication = false;
            });

            services.Configure<RabbitMqConnectionInfo>(configuration.GetSection("LoggerRabbitMqConnectionInfo"));
            services.Configure<IdentityServerInfoOptions>(configuration.GetSection("IdentityServerInfoOptions"));
            services.Configure<ReportBuilderIdentityOptions>(configuration.GetSection("ReportBuilderIdentityOptions"));
            services.Configure<ReportBuilderAlertingEmailOptions>(configuration.GetSection("ReportBuilderAlertingEmailOptions"));

            services.AddDbContext<WebValidatorContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
        }
        public static void RegisterReportBuilderService(this IServiceCollection services, IServiceProvider serviceProvider)
        {
            var identityServerInfoOptions = serviceProvider.GetService<IOptions<IdentityServerInfoOptions>>().Value;
            var reportBuilderIdentityOptions = serviceProvider.GetService<IOptions<ReportBuilderIdentityOptions>>().Value;
            var instance = new ReportBuilderApiClientContext
            {
                GetRootUrl = reportBuilderIdentityOptions.RootUrl,
                ApiGatewayPrefix = "/reportbuilder",
                ApiGatewayUrl = identityServerInfoOptions.ApiGatewayUrl,
                IdentityServerUrl = identityServerInfoOptions.IdentityServerUrl,
                IdentityClientId = reportBuilderIdentityOptions.ClientId,
                IdentityClientSecret = reportBuilderIdentityOptions.ClientSecret,
                AesKey = identityServerInfoOptions.AesKey,
                DisableApiGateway = identityServerInfoOptions.DisableApiGateway,
                DisableIdentityServer = identityServerInfoOptions.DisableIdentityServer,
                Scope = "ReportBuilderApi"
            };

            services.AddSingleton<ReportBuilderApiClientContext>(instance);
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IEmailRepository, EmailRepository>();
            services.AddScoped<ISurveyRepository, SurveyRepository>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IPercolatorService, PercolatorService>();
            services.AddScoped<IPercolatorProcessService, PercolatorProcessService>();
        }
    }
}
