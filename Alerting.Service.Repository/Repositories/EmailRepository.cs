﻿using System;
using System.Threading.Tasks;
using Alerting.Service.Domain.Models;
using Alerting.Service.Repository.Repositories.Contracts;
using Iper.Common.EntityFramework;
using Iper.Common.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using EmailStatusType = Iper.Common.Models.Enums.EmailStatusType;

namespace Alerting.Service.Repository.Repositories
{
    public class EmailRepository : IEmailRepository
    {
        private readonly WebValidatorContext _webValidatorContext;
        private readonly ILogger<EmailRepository> _logger;

        public EmailRepository(WebValidatorContext webValidatorContext, ILogger<EmailRepository> logger)
        {
            _webValidatorContext = webValidatorContext;
            _logger = logger;
        }

        public async Task ProcessEmail(Email email)
        {
            var emailQueue = new EmailQueue
            {
                Subject = email.Subject,
                Body = email.Body,
                EmailTemplateId = email.TemplateId,
                EmailTypeId = email.TypeId,
                FromEmail = email.FromAddress,
                FromName = email.FromName,
                UserId = email.UserId,
                EmailStatusTypeId = (int) EmailStatusType.Created
            };

            try
            {
                await _webValidatorContext.EmailQueues.AddAsync(emailQueue);
                await SaveEmailQueueRecipient(emailQueue, email.ToAddress);
                await _webValidatorContext.SaveChangesAsync();
                _logger.LogInformation("ProcessAnEmail successfully", email);
            }
            catch (Exception e)
            {
                _logger.LogError("ProcessAnEmail error", e, email);
            }
        }

        private async Task SaveEmailQueueRecipient(EmailQueue queue, string toEmail)
        {
            var emailQueueRecipient = new EmailQueueRecipient()
            {
                EmailRecipientTypeId = Convert.ToInt32(Iper.Common.Models.Enums.EmailRecipientType.To),
                Email = toEmail,
                EmailQueue = queue
            };

            await _webValidatorContext.EmailQueueRecipients.AddAsync(emailQueueRecipient);
        }

        public async Task<(string Subject, string Body)> GetEmailTemplate(int templateId)
        {
            var template = await _webValidatorContext.EmailTemplates.FirstOrDefaultAsync(t => t.Id == templateId);
            return (template.Subject, template.Body);
        }

    }
}
