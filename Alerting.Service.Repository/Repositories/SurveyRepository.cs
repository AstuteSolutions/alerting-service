﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Alerting.Service.Domain.Models;
using Alerting.Service.Repository.Repositories.Contracts;
using Iper.Common.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.Extensions.Logging;

namespace Alerting.Service.Repository.Repositories
{
    public class SurveyRepository : ISurveyRepository
    {
        private readonly WebValidatorContext _webValidatorContext;
        private readonly ILogger<EmailRepository> _logger;

        public SurveyRepository(WebValidatorContext webValidatorContext, ILogger<EmailRepository> logger)
        {
            _webValidatorContext = webValidatorContext;
            _logger = logger;
        }

        public async Task<IDictionary<int, int>> GetCompanySurveyMonitored()
        {
            return await _webValidatorContext.SurveyDataSources
                .Where(s => s.IsMonitored)
                .ToDictionaryAsync(s => s.SurveyId, s=> s.CompanyId);
        }

        //Need to add more logic to filter the specific users for a company
        public async Task<IDictionary<int, List<string>>> GetReportBuilderUserEmails()
        {
            return (await (from user in _webValidatorContext.Users
                    join rolesUser in _webValidatorContext.RolesUsers on user.UserId equals rolesUser.UserId
                    join reportUser in _webValidatorContext.ReportUsers on rolesUser.Id equals reportUser.UserId
                    join source in _webValidatorContext.SurveyDataSources on reportUser.AccountUid equals source
                        .AccountUid
                    where source.IsMonitored
                    select new {source.CompanyId, user.UserEmail}).ToListAsync())
                .GroupBy(c => c.CompanyId, c => c.UserEmail)
                .ToDictionary(e => e.Key, e => e.ToList());
        }

        public async Task<SurveyInfo> GetSurveyInfo(int surveyId)
        {
            return await (from account in _webValidatorContext.SurveyAccounts
                join info in _webValidatorContext.CompanyInfoes on account.CompanyId equals info.CompanyId
                where account.SurveyId == surveyId
                select new SurveyInfo
                {
                    SurveyId = surveyId,
                    SurveyName = account.SurveyTitle,
                    CompanyId = account.CompanyId,
                    CompanyName = info.CompanyName
                }).FirstOrDefaultAsync();
        }
    }
}
