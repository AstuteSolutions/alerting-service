﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Alerting.Service.Domain.Models;

namespace Alerting.Service.Repository.Repositories.Contracts
{
    public interface ISurveyRepository
    {
        Task<IDictionary<int, int>> GetCompanySurveyMonitored();
        Task<IDictionary<int, List<string>>> GetReportBuilderUserEmails();
        Task<SurveyInfo> GetSurveyInfo(int surveyId);
    }
}