﻿using System.Threading.Tasks;
using Alerting.Service.Domain.Models;

namespace Alerting.Service.Repository.Repositories.Contracts
{
    public interface IEmailRepository
    {
        Task ProcessEmail(Email email);
        Task<(string Subject, string Body)> GetEmailTemplate(int templateId);
    }
}