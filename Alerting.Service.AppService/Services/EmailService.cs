﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Alerting.Service.AppService.Services.Contracts;
using Alerting.Service.Domain.Models;
using Alerting.Service.Domain.Options;
using Alerting.Service.Repository.Repositories.Contracts;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Alerting.Service.AppService.Services
{
    public class EmailService : IEmailService
    {
        private readonly IEmailRepository _emailRepository;
        private readonly ILogger<EmailService> _logger;
        private readonly ReportBuilderAlertingEmailOptions _emailOptions;

        public EmailService(IEmailRepository emailRepository, ILogger<EmailService> logger, IOptions<ReportBuilderAlertingEmailOptions> emailOptions)
        {
            _emailRepository = emailRepository;
            _logger = logger;
            _emailOptions = emailOptions.Value;
        }

        public async Task<bool> SendEmail(int templateId, IDictionary<string, string> placeHolderKeyValuePair, string[] listOfToEmails = null)
        {
            var template = await _emailRepository.GetEmailTemplate(templateId);

            if (template.Subject is null || template.Body is null) return false;

            var emailBody = await GetReplacedValuesString(template.Body, placeHolderKeyValuePair);
            var subject = await GetReplacedValuesString(template.Subject, placeHolderKeyValuePair);

            return await InsertIntoEmailQueue(templateId, subject, emailBody, listOfToEmails);
        }

        private async Task<string> GetReplacedValuesString(string template, IDictionary<string, string> keyValuePairs)
        {
            var stringBuilder = new StringBuilder(template);
            var result = new Func<string>(() =>
            {
                if (keyValuePairs.Count <= 0) return stringBuilder.ToString();
                foreach (var (key, value) in keyValuePairs)
                {
                    stringBuilder.Replace(key, value);
                }
                return stringBuilder.ToString();
            });
            return await Task.Run(result);
        }

        private async Task<bool> InsertIntoEmailQueue(int templateId, string subject, string body, string[] listOfToEmails)
        {
            try
            {
                if (listOfToEmails?.Length > 0)
                {
                    foreach (var toEmail in listOfToEmails)
                    {
                        await ProcessEmail(templateId, subject, body, toEmail);
                    }
                }
                else
                {
                    await ProcessEmail(templateId, subject, body, _emailOptions.ToAddress);
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError("InsertIntoEmailQueue error", ex);
                return false;
            }
        }

        private async Task ProcessEmail(int templateId, string subject, string body, string toEmail)
        {
            var email = new Email
            {
                TemplateId = templateId,
                TypeId = _emailOptions.TypeId,
                Subject = subject,
                Body = body,
                FromAddress = _emailOptions.FromAddress,
                ToAddress = toEmail,
                FromName = _emailOptions.FromName
            };
            await _emailRepository.ProcessEmail(email);
        }
    }
}
