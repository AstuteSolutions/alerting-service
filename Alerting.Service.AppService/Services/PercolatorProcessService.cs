﻿using Alerting.Service.AppService.Services.Contracts;
using Alerting.Service.Domain.Models;
using Alerting.Service.Repository.Repositories.Contracts;
using Iper.ReportBuilder.Api.Web.Client.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alerting.Service.AppService.Services
{
    public class PercolatorProcessService : IPercolatorProcessService
    {
        private readonly IPercolatorService _percolatorService;
        private readonly ILogger<PercolatorProcessService> _logger;
        private readonly ISurveyRepository _surveyRepository;
        private readonly IEmailService _emailService;
        private const int AlertingServiceEmailTemplateId = 40;
        
        internal List<SurveyInfo> SurveyInfos = new();

        public PercolatorProcessService(IPercolatorService percolatorService,
            ILogger<PercolatorProcessService> logger, 
            ISurveyRepository surveyRepository,
            IEmailService emailService)
        {
            _percolatorService = percolatorService;
            _logger = logger;
            _surveyRepository = surveyRepository;
            _emailService = emailService;
        }

        public async Task Process()
        {
            try
            {
                var companySurveyMonitored = await _surveyRepository.GetCompanySurveyMonitored();
                var companyMonitoredUserEmails = await _surveyRepository.GetReportBuilderUserEmails();

                foreach (var surveyCompany in companySurveyMonitored)
                {
                    var surveyId = surveyCompany.Key;
                    var companyId = surveyCompany.Value;
                    var userEmails = companyMonitoredUserEmails[companyId];

                    var percolatorDocuments = await _percolatorService.GetMatchedDocumentCountAsync(companyId, surveyId);

                    // if percolated alerting queries returns no documents, situation is good, no alerts for this survey
                    if (percolatorDocuments.Data.Count == 0) continue;

                    var surveyInfo = await GetSurveyInfo(surveyId);
                    var placeHolderKeyValuePair = GeneratePlaceHolders(percolatorDocuments, surveyInfo);

                    await _emailService.SendEmail(AlertingServiceEmailTemplateId, placeHolderKeyValuePair,
                        userEmails.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Error from PercolatorProcessService.Process", e);
            }
        }

        private IDictionary<string, string> GeneratePlaceHolders(MatchedQueryResultListQueryHandlerResult percolatorDocuments, SurveyInfo surveyInfo)
        {
            var alertingServicePlaceHolders = new Dictionary<string, string>
            {
                {"[ProjectNumber]", surveyInfo.SurveyId.ToString()},
                {"[SurveyName]", surveyInfo.SurveyName},
                {"[CustomerName]", surveyInfo.CompanyName}
            };

            foreach (var data in percolatorDocuments.Data)
                if (data.QueryName.Contains("CollectionCount", StringComparison.CurrentCultureIgnoreCase))
                    alertingServicePlaceHolders.Add("[CollectionCount]", data.DocumentCount.ToString());

            return alertingServicePlaceHolders;
        }

        private async Task<SurveyInfo> GetSurveyInfo(int surveyId)
        {
            var surveyInfo = SurveyInfos.FirstOrDefault(s => s.SurveyId == surveyId);

            if (surveyInfo is null)
            {
                surveyInfo = await _surveyRepository.GetSurveyInfo(surveyId);
                SurveyInfos.Add(surveyInfo);
            }

            return surveyInfo;
        }
    }
}
