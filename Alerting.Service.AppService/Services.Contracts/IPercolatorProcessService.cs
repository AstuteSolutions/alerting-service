﻿using System.Threading.Tasks;

namespace Alerting.Service.AppService.Services.Contracts
{
    public interface IPercolatorProcessService
    {
        Task Process();
    }
}