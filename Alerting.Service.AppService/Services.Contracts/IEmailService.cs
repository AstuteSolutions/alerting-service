﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Alerting.Service.AppService.Services.Contracts
{
    public interface IEmailService
    {
        Task<bool> SendEmail(int templateId, IDictionary<string, string> placeHolderKeyValuePair, string[] listOfToEmails = null);
    }
}